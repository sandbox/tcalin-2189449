This module add a new imagecache style that allows scaling an image
using percentages as input values.

Install the module as usual. Go to http://example.com/admin/config/media/image-styles
and create a new image style. Then go to http://example.com/admin/config/media/image-styles/edit/your_style
and you will see Percentage Scale in the list of available effects.
